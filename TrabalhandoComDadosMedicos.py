## Python  print()

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

data = pd.read_csv("C:\\Users\\1220080\\Desktop\\Curso Machine Learning\\dados via email\\tarefa_apl_machine_learning\\data.csv")

print(data.shape)

print(  )
print(data.head(6))

print(  )
print( data.drop(["id"], axis = 1) )

print(  )
y = list(data.columns[1:11])
print( y )

print(  )
#features_mean = list(data.columns[1:11])
features_mean = list(data.columns[2:12])

print( "Exibe os 2 primeiro" )
print( data[features_mean].head(2) )

print(  )
print( "Correlação" )
print( data[features_mean].corr() )

print(  )
print( "Gráficos 1" )
plt.figure(figsize = (10, 10))
sns.heatmap(data[features_mean].corr())
print( plt.show() )

print(  )
print( "Gráficos 2" )
plt.figure(figsize = (10, 10))
sns.heatmap(data[features_mean].corr(), annot=True, square = True, cmap = "coolwarm")
print( plt.show() )
print( "Fim Gráfico 2" )

print(  )
print( data["diagnosis"].value_counts() )
print( "Fim Data Diagnosis" )

print(  )
color_dict = { "M": "Red", "B": "Blue" }
colors = data["diagnosis"].map(lambda x: color_dict.get(x))
print( colors.head() )
print( "Fim Cores Lambda" )
'''
print(  )
sm = pd.plotting.scatter_matrix( data[features_mean], c = colors )
print( sm )
print( "Fim variavel sm parte 1" )

print(  )
sm = pd.plotting.scatter_matrix( data[features_mean], c = colors, alpha = 0.4, figsize = ((15, 15)) )
print( sm )
print( "Fim variavel sm parte 2" )
'''
print(  )
print( "Diagnosis: " )
print( data[data["diagnosis"] == "M"].head(2) )

print(  )
print( "Loop em cada coluna/feature de feature_mean: " )
nr_bins = 12
plt.figure(figsize = (15, 15))

for i, feature in enumerate(features_mean):
    rows = int(len(features_mean)/2)
    plt.subplot(rows, 2, i+1)
    sns.distplot(data[data["diagnosis"] == "M"][feature], bins = nr_bins, color = "Red", label = "M")    
    sns.distplot(data[data["diagnosis"] == "B"][feature], bins = nr_bins, color = "Blue", label = "B")    
    plt.legend(loc = "upper right")
    
plt.tight_layout()    
print( plt.show() )

print(  )
print( "Gráfico Boxplot" )
plt.figure(figsize = (15, 15))
for i, feature in enumerate(features_mean):
    rows = int(len(features_mean)/2)
    plt.subplot(rows, 2, i+1)
    sns.boxplot(x = "diagnosis", y = feature, data = data, palette = "Set1")
    
plt.tight_layout()
print( plt.show() )
'''
print(  )
print( "Tipo data:" )
print( type( data[data["radius_mean"] > 20 ] ) )

print(  )
print( "Data Shape" )
print( data[data["radius_mean"] > 20 ].shape )

print(  )
print( data[data["radius_mean"] > 20 ].shape[0] )

print(  )
print( data[data["diagnosis"] == "M" ]["texture_mean"].head(2)
'''

print(  )
from sklearn.model_selection import train_test_split
diag_map = { "M": 1, "B": 0 }
data["diagnosis"] = data["diagnosis"].map(diag_map)
print( data["diagnosis"].value_counts() )
'''
print(  )
print( features_mean )
'''
print(  )
x = data.loc[:, features_mean]
y = data.loc[:, "diagnosis"]
print( "Valor X head(2)" )
print( x.head(2) )

print(  )
print( "Valor Y head(2)" )
print( y.head(2) )

print(  )
print( "Valor x.shape:" )
print( x.shape )

#Para Rodar no Jupter Notebook x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state=42)
print(  )
print( "Predição:" )
x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state=42)
print( x_train.shape,  y_test.shape )

print(  )
#Suporte Vector Machine
from sklearn.svm import SVC, NuSVC, LinearSVC
#SVC
clf = SVC()
clf.fit(x_train, y_train)
prediction = clf.predict(x_test)

#Modelagem Analítica
from sklearn.model_selection import cross_val_score
from sklearn.metrics import accuracy_score
scores = cross_val_score(clf, x, y, cv = 5)

print(  )
print( "Variavel Array SVC scores:" )
print( scores )

print(  )
#Comparação de resultados
print( "Comparação de Resultados:" )
print( accuracy_score(prediction, y_test) )
print( "Validação Cruzada:" )
print( np.mean(scores) )
print( "Junção:" )
print( accuracy_score(prediction, y_test), np.mean(scores) )

print(  )
#NuSVC
clf = NuSVC()
clf.fit(x_train, y_train)
prediction = clf.predict(x_test)
scores = cross_val_score(clf, x, y, cv = 5)

print(  )
print( "Variavel Array NuSVC scores:" )
print( scores )

print(  )
#Comparação de resultados
print( "Comparação de Resultados:" )
print( accuracy_score(prediction, y_test) )
print( "Validação Cruzada:" )
print( np.mean(scores) )
print( "Junção:" )
print( accuracy_score(prediction, y_test), np.mean(scores) )

print(  )
#LinearSVC
clf = LinearSVC()
clf.fit(x_train, y_train)
prediction = clf.predict(x_test)
scores = cross_val_score(clf, x, y, cv = 5)

print( "Variavel Array LinearSVC scores:" )
print( scores )

print(  )
#Comparação de resultados
print( "Comparação de Resultados:" )
print( accuracy_score(prediction, y_test) )
print( "Validação Cruzada:" )
print( np.mean(scores) )
print( "Junção:" )
print( accuracy_score(prediction, y_test), np.mean(scores) )

#Modelagem KNN
print(  )
print( "Classe KNeighborsClassifier:" )
from sklearn.neighbors import KNeighborsClassifier
clf = KNeighborsClassifier()
clf.fit(x_train, y_train)
prediction = clf.predict(x_test)
print( prediction )

print(  )
print( "Comparação de Resultados:" )
print( accuracy_score(prediction, y_test) )
print( "Validação Cruzada:" )
print( np.mean(scores) )
print( "Junção:" )
print( accuracy_score(prediction, y_test), np.mean(scores) )

#Árvore de Decisão / Decision Trees
print(  )
print( "Classe DecisionTreeClassifier:" )
from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()
clf.fit(x_train, y_train)
prediction = clf.predict(x_test)
print( prediction )

print(  )
print( "Comparação de Resultados:" )
print( accuracy_score(prediction, y_test) )
print( "Validação Cruzada:" )
print( np.mean(scores) )
print( "Junção:" )
print( accuracy_score(prediction, y_test), np.mean(scores) )

#Árvore Aleatório / Random Trees
print(  )
print( "Classe RandomForestClassifier:" )
from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier()
clf.fit(x_train, y_train)
prediction = clf.predict(x_test)
print( prediction )

print(  )
print( "Comparação de Resultados:" )
print( accuracy_score(prediction, y_test) )
print( "Validação Cruzada:" )
print( np.mean(scores) )
print( "Junção:" )
print( accuracy_score(prediction, y_test), np.mean(scores) )

#Gradient Boosting
print(  )
print( "Classe GradientBoostingClassifier:" )
from sklearn.ensemble import GradientBoostingClassifier
clf = GradientBoostingClassifier()
clf.fit(x_train, y_train)
prediction = clf.predict(x_test)
print( prediction )

print(  )
print( "Comparação de Resultados:" )
print( accuracy_score(prediction, y_test) )
print( "Validação Cruzada:" )
print( np.mean(scores) )
print( "Junção:" )
print( accuracy_score(prediction, y_test), np.mean(scores) )
