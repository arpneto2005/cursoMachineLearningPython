salarios = { "Marcos": [2300, 3450, 9800], "Vera": [1100, 9800, 7500], "Lucia": [5600, 7200, 1650] }

print(salarios)
print("Marcos:", salarios["Marcos"])

import pandas as pd

salario_marcos = pd.Series(salarios["Marcos"])
print(salario_marcos)

print()

salario_marcos.index = [ "Jan", "Fev", "Mar" ]
print(salario_marcos)

print()

salario_df = pd.DataFrame(salarios)
print(salario_df)

print()

salario_df.index = [ "Jan", "Fev", "Mar" ]
print(salario_df)

print()

print(salario_df["Lucia"])

print()

print(salario_df.mean(axis = 0))

print()

print(salario_df.mean(axis = 1))

print()

print(salario_df.info())