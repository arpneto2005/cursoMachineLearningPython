kaggle.com/arpneto2005

Qual seu neg�cio?
Neg�cio: Alimentos e Bebidas

Qual a pergunta Mais cr�tica para seus neg�cios?
Pergunta Cr�tica: Onde abrir um neg�cio para atingir o maior n�mero de clientes

Qual a necessidade maior atual do seu neg�cio?
Necessidade Maior: Ter o maior variedades de estoque

Como voc� acha que seus dados podem lhe ajudar a responder a estas perguntas?
Saber que tipo de clientes aganger, saber quais produtos ter em maior quantidade, 
para n�o haver desperd�cio e minimizar ao m�ximo
o desped�cio de dinheiro e por conseguencia produtos.

1. Defini��o da pergunta estrat�gica.
2. Identifica��o dos dados.
3. Coleta de pr�-processamento dos dados para uso no projeto.
4. Explora��o e visualiz��o dos dados.
5. Modlagem anal�stica.
6. Avalia��o da modelagem.
7. Implementa��o da Modelagem nos neg�cios.


Exemplo C�digo Numpy

import numpy as np

pessoas = { "A": 1000, "B": 2000, "C": 3000, "D": 4000, "E": 5000, "F": 1500, "G": 2500, "H": 3500, "I": 4500, "J": 5500, "K": 1800, "L": 2800, "M": 3800, "N": 4800, "O": 5800}

x = np.array( list(pessoas.values()) )

media = x.mean()
maximo = x.max()
menor = x.min()
pos_maior_elem = x.argmax()


print( "Media: ", media )
print( "M�ximo: ", maximo )
print( "M�nimo: ", menor )
print( "Posi��o Maior Elemento: ", pos_maior_elem )