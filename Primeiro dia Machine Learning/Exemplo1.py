# -*- coding: utf-8 -*-
"""
Created on Sat May 26 16:02:56 2018

@author: visitante
"""

import numpy as np

pessoas = { "A": 1000, "B": 2000, "C": 3000, "D": 4000, "E": 5000, "F": 1500, "G": 2500, "H": 3500, "I": 4500, "J": 5500, "K": 1800, "L": 2800, "M": 3800, "N": 4800, "O": 5800}

x = np.array( list(pessoas.values()) )

media = x.mean()
maximo = x.max()
menor = x.min()
pos_maior_elem = x.argmax()
p = np.unravel_index(np.argmax(x), np.shape(x))


print( "Media: ", media )
print( "Máximo: ", maximo )
print( "Mínimo: ", menor )
print( "Posição Maior Elemento: ", p)

#print(x)