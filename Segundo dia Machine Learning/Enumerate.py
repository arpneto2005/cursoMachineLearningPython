# 2. Enumerate     print()

x = [456, 30, 2019, "Marcos"]
for cada_x in x:
    ##print(x)
    print(cada_x)
   
print()

for cada_x in enumerate(x):
    print(cada_x)
    
print()

x = {"Marcos": [24, 57, 98], "Luiz": [57, 89, 35]}

print("Valor")
for each_x in enumerate(x.values()):
    print(each_x)

print()

print("Chaves")
for each_x in enumerate(x.keys()):
    print(each_x)
    
print()

print("Itens")
for each_x in enumerate(x.items()):
    print(each_x)