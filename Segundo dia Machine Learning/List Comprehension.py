# 3. LIst Comprehension   print()
    
notas = [8.5, 7.9, 7.8]
print("Notas")
for nota in notas:
    print(nota)
    
print()

print("Notas")
print([ nota for nota in notas ])

print("Nota + 1")
print([ nota+1 for nota in notas ])

print("Notas > 8")
print([ nota  for nota in notas if nota > 8 ])