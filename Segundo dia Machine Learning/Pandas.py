# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 09:32:42 2018

@author: 1220080
"""

import pandas as pd

print()

salarios_dict = { "Analista": [12700, 14300, 32000, 27500, 11000, 18400],
                     "Engenheiro": [9800, 18300, 21100, 8700, 6800, 9000],
                     "Gerente": [16700, 11200, 19300, 14500, 12800, 20600],
                     "Cidade": ["Fortaleza", "São Paulo", "Florianopóles", "Florianopóles", "São Paulo", "Fortaleza"] }

salarios_df = pd.DataFrame(salarios_dict)
print( salarios_df )

print()

print("Agrupamento de soma por cidade: ")
print( salarios_df.groupby("Cidade").sum() )

print()

print("Agrupamento de média por cidade: ")
print( salarios_df.groupby("Cidade").mean() )