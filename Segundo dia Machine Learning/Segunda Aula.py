# Numpy

import numpy as np
print()

print(np.mean( [23, 56, 34] ))

print()

print("Usando nan:")
print(np.mean( [23, 56, 34, np.nan] ))

print()

print("Negando nan:")
print( np.nanmean( [23, 56, 34, np.nan] ) )

print()

print("Diminuindo casas decimais:")
x = np.around( [ 34.46472826 ], decimals = 1 )
print(x)

print()

print("Diminuindo casas decimais em mais de um array:")
y = np.around( [34.46472826, 82.6462927], decimals = 1 )
print(y)