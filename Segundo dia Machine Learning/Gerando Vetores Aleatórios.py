# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 09:12:25 2018

@author: 1220080
"""
import numpy as np

# Gerando Vetores Aleatórios

x = np.random.RandomState(42)

print(x)

print()

y = np.random.randint(1, 61, 6)
print(y)

print()

z = np.random.randint(1, 61, (6,2))
print(z)

print()

c = np.random.rand(3)
print(c)

print()

