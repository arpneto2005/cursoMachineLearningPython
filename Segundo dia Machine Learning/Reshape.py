# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 09:21:19 2018

@author: 1220080
"""

# Reshape

import numpy as np

x = np.random.randint( 1, 20, (3, 2) )
print(x)

print()

print(x.reshape(6, 1))