# Python 

# 1. Tuplas  --   print()

x = (20, 34)

print(x[0])

print()

print(type(x))

print()

print(len(x))

print()

x = (2, 3)
y = (34, 98)

print(x + y)

print()

x = (23, "Marcos", 45)

print(x)

print()