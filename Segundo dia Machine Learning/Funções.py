# Funções   print()

def quadrado(x):
    return x**2

print("Eleva ao quadrado: ")
print(quadrado(3))

print("Soma")

def soma(x, y):
    return x + y

def soma_com10(x, y = 10):
    return x + y